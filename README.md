# GTAV - Enhanced Taxi Missions

[Enhanced Taxi Missions [.NET] (Original)](https://www.gta5-mods.com/scripts/enhanced-taxi-missions) 
## IMPORTANT NOTE
This MOD was originally created and maintained by @LCBuffalo until v1.4.
Unfortunately, due to no updates to the mod, and no responses after contacting him on GTAForums and GTA5-Mods, I've started working on the mod by adding new features and fixing different bugs, in the end releasing v1.5.
Without his extensive work, we wouldn't have had this mod, so @LCBuffalo thank you for your time in creating this.

As a side not, I'm pretty much a beginner in .NET, with just a few days experince in it, experince gained while working at release v1.5 and v1.51. Therefore I'm continuosly learning new things and try to do implementations as good as I can, currently, so if you find any bugs or have any suggestion, please let me know as I'll be more than happy to acquire even more knowledge.

Lots of thanks to @LCBuffalo, multiple devs who came with answers when I had questions ( you know yourselves ), and other people who came with suggestions!




## DESCRIPTION
GTA V's default taxi missions aren't bad, but they lack variety. It seems like Rockstar only used about 10 pre-defined pick-up and drop-off locations.

The goal of Enhanced Taxi Missions is to do it better and bring more variety to your taxi-driving adventures! See the feature list further down for more information.



## FEATURES
- Rating System! Now passengers can rate you, and give you a rating between 1.00 and 5.00 based on the way you have driven, the speed you have driven, and if you have hit any peds/animals/cars! Some of the customers that have a rating lower than 2.0, might run away and not pay the ride! Customer and player rating will also be shown with different colours, based on how low or high is it. The player can not rate the clients to keep things simple, and also does not provide much of a point to do that as the customers will always have different ratings. The system can be either disabled or enabled.
- Controller support! D-PAD, back buttons and triggers can be customized.
- Now you can use vehicles with at least 2 seats! This includes every vehicle with at least 2 seats ( at least 1 passenger seat available ), and also motorbikes/quad bikes as well - in this case, the ped won't be knocked off the vehicle, except when it dies. When the mod is toggled while in a 2 seat vehicles, it will automatically spawn only one passenger for pick up.
- When on motorbikes/quad the passengers will randomly put on helmets. Note that the helmet can not be put on while driving, so a small stop is necessary.
- Random number of passengers for each trip ( between 1 and 3 ).
- 950+ individual pick-up and drop-off locations.
- Pick-up locations and destinations change depending on the time of day ( people go to work in the morning, home in the evening, to clubs and restaurants at night, etc ).
- Player is paid based on distance between pick-up and drop-off locations. Base fare is $6, and every mile driven is $20.
- Random peds on the street may hail player if driving a taxi cab. ( This may still be a bit buggy, sometimes peds are on balconies and can't get down. They will eventually warp into the car. )
- Passengers will not get into a car if it's more than 25% damaged. Any collisions that occur during your trip will cause your passengers to pay less. ( Minor scrapes and bumps won't be noticed, unless there are a lot of them. )
- The customers are more reactive! If you shoot out of the car or being shot at while driving to destination/picking them up, will cause them to abort the mission and flee.



## IN-GAME
Toggle missions:
- Keyboard: Press the "T" ( TOGGLE_KEY ) key.
- Controller: Simultaneously press "PadRight" ( TOGGLE_CONTROLLER_1 ) and "LB" ( TOGGLE_CONTROLLER_2 ).

Cancel Rides ( Can't do this if the customer is already in the vehicle - you can toggle the ETM off and on, though ):
- Keyboard: Press the "R" ( CANCEL_RIDE ) key.
- Controller: Simultaneously press "PadRight" ( CANCEL_RIDE_CONTROLLER_1 ) and "RB" ( CANCEL_RIDE_CONTROLLER_2 ).

If a passenger gets stuck while trying enter the vehicle, horn to have the passenger retry entering the car. If they still won't move, just wait, they will eventually warp into the car ( default timeout is set to 8 seconds ). Pressing the horn key on TAXI is safe, as it won't trigger the stock missions.

Once you've picked up a passenger, a countdown timer will appear on the UI. As long as it's green, your tip will be 40% of the fare. Once the timer turns yellow, your tip will start to decrease. Once the timer reaches zero, you will not receive a tip. However, you can still complete the mission and receive the base fare.



## PREREQUISITES:
- .NET Framework 4.8 runtime ( don't know for sure if it is necessary, but its better to have it ):
  https://dotnet.microsoft.com/download/dotnet-framework/net48
- Script Hook V:
  https://www.gta5-mods.com/tools/script-hook-v
- Script Hook V Dot Net + the dependencies:
  https://www.gta5-mods.com/tools/scripthookv-net



## INSTALLATION:

- If you update from a version previous to 2.0 be sure that EnhancedTaxiMissions.vb is removed, as now it will be compiled. In any case, If you have an old .dll or .vb version of this mod installed, remove it.
- Place the EnhancedTaxiMissions.dll and EnhancedTaxiMissions.ini files into the "scripts" folder in your main GTAV directory ( Create the folder if it doesn't exist ).



## CUSTOMIZATION:
Edit the values in EnhancedTaxiMissions.ini to customize the experience to your taste.

Please note that if the script can't find the EnhancedTaxiMissions.ini file, it will revert to the default values.

Keyboard valid keys are "A" through "Z", "0" through "9", and "F1" through "F12".

Controller valid buttons are "LB", "RB", "LT", "RT", "LS", "RS", "PadUp", "PadDown", "PadLeft", "PadRight".


- TAXI_DRIVER_TITLE:        --- Defaults to "Taxi Driver"       --- Title when doing missions with a taxi.
- LIMOUSINE_DRIVER_TITLE:   --- Defaults to "Limousine Driver"  --- Title when doing missions with the stretch.
- GENERIC_DRIVER_TITLE:     --- Defaults to "Ride Share Driver" --- Title when doing missions with a generic vehicle.
- TOGGLE_KEY:               --- Defaults to "T"                 --- Mission start keyboard key.
- TOGGLE_CONTROLLER_1:      --- Defaults to "PadRight"          --- Mission start controller button 1.
- TOGGLE_CONTROLLER_2:      --- Defaults to "LB"                --- Mission start controller button 2.
- CANCEL_RIDE_KEY:          --- Defaults to "R"                 --- Cancel rides keyboard key. Can't cancel if customer is in vehicle.
- CANCEL_RIDE_CONTROLLER_1: --- Defaults to "PadRight"          --- Cancel rides controller button 1.
- CANCEL_RIDE_CONTROLLER_2: --- Defaults to "RB"                --- Cancel rides controller button 2.
- RATING_SYSTEM:            --- Defaults to "1" ( on )          --- Enable or disable the rating system. Disabling the system will disable different ped scenarios based on their rating, and the rating completely. Can be 1 for enabled or 0 for disabled.
- PLAYER_RATING:            --- Defaults to 5.00                --- The initial player rating to start with when RATING_SYSTEM enabled. Can be any value between 1.00 to 5.00 ( e.g. 4.63 ).
- UNITS:                    --- Defaults to "KM"                --- Valid options are "KM" for kilometers and "MI" for miles.
- FARE_PER_MILE:            --- Defaults to "20"                --- Amount the player earns per mile driven with a customer on board.
- AVERAGE_SPEED:            --- Defaults to "65"                --- Number used to calculate how quickly you need to arrive at your destination in order to receive a tip. Higher numbers mean you need to drive faster. 80 is a good number for those of you who want to drive as fast as possible. 20 is a good number for those of you who like to follow the rules while driving. Change it to suit your playstyle.
- ENTER_TIMEOUT:            --- Defaults to 8 seconds           --- The timeout value in seconds for the passenger to automatically warp into the vehicle.
- HAZARD_LIGHTS:            --- Defaults to "0" ( off )         --- Enter 1 to use flashing hazard lights while you're waiting for your passengers to enter and exit the vehicle or 0 to disable.
- AUTOSAVE:                 --- Defaults to "0" ( off )         --- Enter 1 to enable autosaving after each successfully completed fare or 0 to disable.



## KNOWN BUGS:
- Starting ETM while a stock one is in progress, will keep the default taxi missions disabled until you reload a save or restart the game. This seems to be due to a bug in the game itself, probably. If the stock missions matter for you, be sure that you don't have any stock mission ongoing while toggling ON the ETM.
- If peds are startled while walking to your car ( e.g. nearly run over by another driver ), they may lose focus and just stand around. Honk your horn to reset their pathfinding algorithm. They will eventually warp into your car if it takes them too long to get to you or if there is an obstruction.
