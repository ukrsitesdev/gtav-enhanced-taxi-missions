### CHANGELOG

2.2:
- Features:
  - Controller buttons as D-PAD, back buttons and triggers can be customized now. Therefore new customizations values have been added ( TOGGLE_CONTROLLER_1, TOGGLE_CONTROLLER_2, CANCEL_RIDE_CONTROLLER_1, CANCEL_RIDE_CONTROLLER_2 ). Valid options are "LB", "RB", "LT", "RT", "LS", "RS", "PadUp", "PadDown", "PadLeft", "PadRight".
- Bugfix:
  - Fixed rating overlapping on resolutions <= 1920x1080.
  - Fixed 2 locations where pickUp/dropOff coordinate was unfit ( Puerto Del Sol Yacht Club and Guido's Takeout ). Thanks @GamerBoi42!
  - Fixed drop off timer when it is higher than 1 hour. Thanks again to @GamerBoi42 for pointing out this totally obvious issue that I've missed!
- Refactor:
  - Changed some customization names ( TOGGLE_KEY, CANCEL_RIDE_KEY, ENTER_TIMEOUT).
  - Made some variables public, to keep them arranged a bit.

2.1:
- Features:
  - Rating System! Now passengers can rate you, and give you a rating between 1.00 and 5.00 based on the way you have driven, the speed you have driven, and if you have hit any peds/animals/cars! Some of the customers that have a rating lower than 2.0, might run away and not pay the ride! Customer and player rating will also be shown with different colours, based on how low or high is it.The player can not rate the clients to keep things simple, and also does not provide much of a point to do that as the customers will always have different ratings. The system can be either disabled or enabled in the .ini config file with "RATING_SYSTEM" set to 0 or 1. Big thanks to the user who gave me some recommandations about this system. Unfortunately I can not find the message from you anymore, but if you see this, big thanks for the insights!
  - Controller support has been added! Press LS and RS on the controller at the same time to toggle the ETM and D-PAD Right and RB buttons simultaneously to cancel a ride. For the moment these are not configurable. Tested only with Xbox controllers. Should work with others as well.
  - Ability to cancel a ride. This will work with both rating system enabled or disabled.
  - Add customer comments on overspeed, car crashes, and running over pedestrians or animals.
- Bugfix:
  - Fixed the time left for drop off. Instead of seconds, now it displays in MM:SS format.
  - Fixed 3 locations where peds were spawning totally wrong ( Robert Dazzler, Kayton Banking and can't remember the third ).
  - Fixed the script crash that occured during the player exiting the car while the customers were getting to the vehicle. Now if the player exits on pick up, the customers will wait for him to get in the car.
  - Fixed the script crash that occured during car exit on customers drop off.
- Refactor:
  - Re-arrange lots and lots of things


2.0:
- Features:
  - MAJOR UPDATE: SWITCHED TO SHVDN V3 - rewritten a big part of the script to update the calls to ScriptHookVDotNet V3, as it gives much more room for features and bug fixes. Together with this, now the script will be compiled, in order to remove from SHVDN the overhead of compiling. Don't worry, the script is, and will always be open source, as @LCBuffalo seems to have intended. It is present here: https://gitlab.com/g2576/gtav-enhanced-taxi-missions . As these are major changes, I decided to jump directly to V2.0. With this update, I recommend to open issues in gitlab if you find any bug, or come with merge requests if you have any recommendations.
  - The customers are more reactive! If you shoot out of the car or you're being shot at while driving to destination/picking them up, will make them to abort the mission and flee. For this feature, I kindly ask you to provide feedback, so if you consider it irritating, I will add the posibility to disable this, in the future updates.
  - Increase the ammount of money some random peds will carry with them. This doesn't mean a higher pay for the cab, but maybe you can convince them to give it to you? :)
- Bugfix:
  - The stock taxi missions will be disabled only while doing ETM, so pressing the horn will not activate them unless you stop ETM. With this update, I have now removed the option for alternative Taxi horn. BUG: - Starting ETM while a stock one is in progress, will keep the default taxi missions disabled until you reload the save or restart the game. This seems to be due to a bug in the game itself, probably. If the stock missions matter for you, be sure that you don't have any stock mission ongoing while toggling ON the ETM.
  - The GPS route will disappear no more, as well as it will stop flickering. This took quite a while to fix, as I discovered a new bug where the route will disappear after accesing certain parts of the map. The route is now set with the waypoint, therefore setting a waypoint on the map while doing ETM will be disabled.
  - Pedestrians around the customers and the customers as well can now be shot, at pick up. Nobody stops you now, but why would you do that?
  - Fix "Bay City Ave" location where the customer was spawning out of the map.
  - Motorbikes should not have any issues with the pick up marker anymore. Maybe conflicted with other mods?
- Refactor:
  - Rewrite all V2 calls to V3 SHVDN
  - Redo some of the classes and fix some unclosed tasks which kept memory occupied


1.52:
- Features:
  -  NOTE: REMOVED IN V2.0 - Added an alternative key for taxi horn, to avoid starting the stock missions ( default car horn key will still be available ). Can be set in ".ini" file as "TAXI_HORN_KEY". This is somewhat a workaround as I haven't figured out yet how to disable stock taxi missions. Due to how the native call for this works, the sound will only play briefly, so you can hold the key pressed down for the car to horn multiple times.
  - "GET_IN_TIMEOUT" can now be set in seconds instead of milliseconds
  - Changed blips color, now its orange when picking up a customer, and blue when driving customer to destination
  - If the doors wont't be completely closed by the customers, due to animation issues in ped anims, they will be automatically shut off when driving for another pick up
- Bugfix:
  - Now setting "AVERAGE_SPEED" to 0 won't crash the script anymore. In case it's 0 or a negative value, it will default to "1"
  - If you have or reach over the maximum amount of money ( $2,147,483,647 ) doing enhanced taxi missions won't make the script crash anymore. In this case the value will always remain the max ( $2,147,483,647 )
- Refactor:
  - Refactor some logic
- Docs:
  - Update docs and correct "AVERAGE_SPEED" default value

1.51:
- Features:
  - Add ragdoll state of ped in debug mode
- Bugfix:
  - Prevent the ped from falling from the bike/quad, no matter the driving speed and collision.

1.5:
- Features:
  - Now you can use vehicles with at least 2 seats! This includes every vehicle with at least 2 seats ( at least 1 passenger seat available ), and also motorbikes/quad bikes as well - in this case, the ped won't be knocked off the vehicle, except when it dies. When the mod is toggled while in a 2 seat vehicles, it will automatically spawn only one passenger for pick up.
  - When on motorbikes/quad the passengers will randomly put on helmets. Note that the helmet can not be put on while driving, so a small stop is necessary.
  - Decreased the default warp timeout to 8 seconds, instead of 20 - can be manually specified in the ".ini" config.
  - Changed configurable options and added more: TAXI_DRIVER_TITLE, LIMOUSINE_DRIVER_TITLE, GENERIC_DRIVER_TITLE, GET_IN_TIMEOUT
  - Added more debug values.
  - Changed order of passengers for getting in, to support two seat vehicles.
  - Changed texts from "car" to "vehicle" as motorbikes, and other vehicles are supported for missions as well.
  - Changed the default toggle key to "T". This can be also configured in ".ini" file.
- Bugfix:
  - The issue with specific peds not being able to exit the vehicle at drop off is now fixed. ( hopefully )
  - Now the script can start without the ".ini" config file, as previously it generated value conversions errors if it wasn't present. In this case, it will use the predefined values.
  - Corrected some code values and also remove some unnecessary lines.
- New bugs:
  - When using motorbikes, at passenger pick up, sometimes the ped will not trigger the movement towards the vehicle. Current workaround is to move closer to the ped, or just try moving slightly near the pick up pin.

1.4:
- Bugfix: Fixed the script crash that occurred when you exit your vehicle while the mission is active: You now have 30 seconds to return to your vehicle if you exit it. After that, your passengers will lose their patience and leave.
- Bugfix: Script shouldn't crash when any of your customers die. Instead, the mission will fail, any remaining passengers will flee, and you won't get paid. It's up to you to figure out what to do with the dead bodies in your car - new customers won't like riding alongside a corpse...
- Bugfix: (Mostly) fixed the disappearing GPS route bug: The route will occasionally still disappear, but it will re-appear again after a short while. Side effect: The GPS route flashes periodically.
- Bugfix: Missions crashed if playing for an extended period of time because ped task sequences weren't being cleaned up.
- Some UI improvements:
  - The pick-up and destination information now includes the neighborhood name.
  - Nicer looking payment notifications.

1.31:
- Removed vehicle limitations. You can now use any vehicle in the game as long as it has room for three passengers. Some cars, like the Sentinel Coupe, have four seats but two doors, so passengers will warp into the back seats.
- Re-wrote the route generation algorithm. The script shouldn't crash anymore when there aren't any passenger pick-up locations nearby.
- Added .ini option to enable/disable hazard lights while waiting for passengers to enter/exit the vehicle
- 100 more locations

1.3:
- Passengers won't panic anymore when two or three of them get into your vehicle.
- Passengers won't get in your car if it's too dirty or damaged.
- Passengers will pay less if you damage the car too much during your trip.
- More locations added. Total is now 815.

1.2:
- You can now be a pretend-Uber driver! Taxi missions are enabled for most four-door cars in the game.
- Hopefully fixed a bug where animals were selected as passengers. Hard to test for this, so let me know if it still happens.
- Moooooore locations!

1.1:
- Blue chevron markers now show where to stop your car to pick up and drop off passengers
- Added a ton of new destinations, mostly around Vespucci